package org.zimbuf.integrity.ai;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrityTest {

    @Test
    public void failureMessageTest() {
        var integrity = new Integrity();
        assertEquals("I do not support that query", integrity.generateMessage(""));
    }

    @Test
    public void whatDoYouQueryTest() {
        var integrity = new Integrity();
        assertEquals("it is my opinion that it is a good server os",
                integrity.generateMessage("What do you think of Linux?"));
    }

    @Test
    public void howDoYouMakeQueryTest() {
        var integrity = new Integrity();
        assertEquals("it is created by a mommy and daddy that love each other very much",
                integrity.generateMessage("How do you make a baby?"));
    }

    @Test
    public void howDoYouFeelTest() {
        var integrity = new Integrity();
        assertEquals("it is their feelings that you are agitating",
                integrity.generateMessage("How do you feel?"));
    }

    @Test
    public void whatDoYouThink() {
        var integrity = new Integrity();
        assertEquals("it is my opinion that it runs C# well",
                integrity.generateMessage("What do you think of Windows?"));
    }
}
