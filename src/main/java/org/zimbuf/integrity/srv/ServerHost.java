/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.srv;

import org.zimbuf.integrity.ai.Integrity;
import org.zimbuf.integrity.util.TernaryMutableMap;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;

@Log4j2
public class ServerHost extends Thread {
    private Server[] servers;
    private TernaryMutableMap<Long, Integer, Integer> ledger;
    private Integrity Integrity;
    private static int SLEEP_INTERVAL = 5000;
    private static int SERVER_ACCEPTABLE_TIMEOUT = 600000;
    private boolean shouldRun;

    public ServerHost(Integrity Integrity, int serverCount, int startingPort) {
        this.Integrity = Integrity;
        this.servers = this.initializeServers(startingPort, serverCount);
        this.ledger = new TernaryMutableMap<>();
        this.shouldRun = true;
    }

    public void run() {
        this.kickoffServers();
        while(this.shouldRun) {
            this.checkForStuckServers().restart();
            this.sleepGracefully((long) ServerHost.SLEEP_INTERVAL);
        }
        this.killAll();
    }

    private Server[] initializeServers(int startingPort, int serverCount) {
        Server[] servers = new Server[serverCount];
        for(int x = 0; x < serverCount; x++) {
            servers[x] = new Server(startingPort+x, this.Integrity);
        }
        return servers;
    }

    private void sleepGracefully(Long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            this.shouldRun = false;
        }
    }

    private void killAll() {
        for(Server server : this.servers) {
            log.info("Closing server " + server.getId());
            server.setShouldRun(false);
            server.interrupt();
            try {
                server.getSocket().close();
            } catch(IOException e) {
                log.error("CANT CLOSE");
            }
        }
        log.info("Server host shut down");
    }

    private StuckServers<Long, Server> checkForStuckServers() {
        StuckServers<Long, Server> stuck = new StuckServers<>();
        for(Server server : this.servers) {
            if (!server.getServed().equals(this.ledger.getValue(server.getId()))) {
                this.ledger.put(
                        server.getId(),
                        server.getServed(),
                        0
                );
            } else if (server.getServed().equals(this.ledger.getValue(server.getId())) &&
                    this.ledger.getTimes(server.getId()) > ServerHost.SERVER_ACCEPTABLE_TIMEOUT) {
                stuck.put(server.getId(), server);
            } else {
                this.ledger.put(
                        server.getId(),
                        this.ledger.getValue(server.getId()),
                        this.ledger.getTimes(server.getId()) + ServerHost.SLEEP_INTERVAL
                );
            }
        }
        return stuck;
    }

    private void kickoffServers() {
        for(Server srv : this.servers) {
            srv.start();
        }
    }
}
