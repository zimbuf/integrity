/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.srv;

import org.zimbuf.integrity.ai.Integrity;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Log4j2(topic="Root")
public class Servlet {
    private InputStream input;
    private OutputStream output;
    private Integrity Integrity;
    private long id;
    private int port;
    private boolean connected = true;

    public Servlet(InputStream input, OutputStream output, Integrity Integrity, long id, int port) {
        this.input = input;
        this.output = output;
        this.id = id;
        this.port = port;
        this.Integrity= Integrity;
    }

    public void handleClient() {
        while (this.connected) {
            Request request = listenForRequest();
            Response response = this.askIntegrity(request);
            this.sendResponseToClient(response);
        }
    }

    public void cleanUp() {
        try {
            log.info("Servlet " + this.id + " closing channel on port " + this.port);
            input.close();
            output.close();
        } catch (IOException e) {
            log.error("Could not close session to client on servlet" + this.id + " for reason " + e.toString());
        }
    }

    private Request listenForRequest() {
        Request req = null;
        try {
            if (this.connected) {
                log.info("Servlet " + this.id + " listening for requests on port " + this.port);
                req = new Request(this.input.readNBytes(Request.BYTES));
            }
        } catch (IOException e) {
            log.error("Failure to retrieve request for servlet: " + this.id + " on port " + this.port + " for reason: " + e.toString());
            this.connected = false;
        }
        return req;
    }

    private Response askIntegrity(Request question) {
        if(question != null && this.connected) {
            String answer = this.Integrity.generateMessage(question.getRequest());
            return new Response(answer);
        }
        else {
            return new Response("I do not currently support that operation");
        }
    }

    private void sendResponseToClient(Response response) {
        try {
            if (this.connected) {
                log.info("Servlet " + this.id + " writing response to output stream for port " + this.port);
                this.output.write(response.getResponse());
            }
        } catch (IOException e) {
            log.error("Failed to send response to client on servlet: " + this.id + " for reason: " + e.toString());
            this.connected = false;
        }
    }
}
