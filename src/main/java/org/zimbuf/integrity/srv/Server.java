/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.srv;

import org.zimbuf.integrity.ai.Integrity;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

@Log4j2(topic="Root")
public class Server extends Thread {
    private Integrity Integrity;
    private int port;
    @Getter
    private Integer served;
    @Setter
    private boolean shouldRun;
    private boolean shouldSeekClients;
    @Getter
    private ServerSocket socket;

    public Server(int port, Integrity Integrity) {
        super();
        this.port = port;
        this.Integrity = Integrity;
        this.served = 0;
        this.shouldRun = true;
        this.shouldSeekClients = true;
    }

    @Override
    public void run() {
        while(this.shouldRun) {
            this.socket = this.initializeServer(this.port);
            this.served = 0;
            while (this.shouldSeekClients) {
                Socket client = this.initializeClient(this.socket);
                InputStream input = this.initializeInputStream(client);
                OutputStream output = this.initializeOutputStream(client);
                Servlet comm = this.initializeServlet(input, output);
                this.handleClient(comm);
                this.cleanUpClient(comm);
                this.closeClient(client);
                this.served++;
            }
            this.closeServer(this.socket);
        }
    }

    private ServerSocket initializeServer(int port) {
        ServerSocket sock = null;
        try {
            log.info("Creating server socket for servlet " + this.getId() + " on port " + this.port);
            sock = new ServerSocket(port);
        } catch (BindException e) {
            return this.initializeServer(port + 1);
        } catch (IOException e) {
            log.error("Thread failed to start for reason: " + e.toString());
            this.shouldSeekClients = false;
        }
        return sock;
    }

    private Socket initializeClient(ServerSocket sock) {
        Socket client = null;
        try {
            log.info("Opening client for servlet " + this.getId() + " on port " + this.port);
            client = sock.accept();
        } catch ( SocketException e ) {
            //Assume this is because of the ServerHost closing the server on cleanup
            log.info("Server " + this.getId() + " accepting close request. Exiting");
            this.shouldSeekClients = false;
        } catch (IOException e) {
            log.error("Failed to initialize client on server: " + this.getId() + " for reason: " + e.toString());
            this.shouldSeekClients = false;
        }
        return client;
    }

    private InputStream initializeInputStream(Socket client) {
        InputStream input = null;
        if(this.shouldSeekClients) {
            try {
                log.info("Servlet " + this.getId() + " opening input stream for port " + this.port);
                input = client.getInputStream();
            } catch (IOException e) {
                log.error("Failure to retrieve input stream for server: " + this.getId() + " for reason: " + e.toString());
                this.shouldSeekClients = false;
            }
        }
        return input;
    }


    private OutputStream initializeOutputStream(Socket client) {
        OutputStream output = null;
        if(this.shouldSeekClients) {
            try {
                log.info("Servlet " + this.getId() + " getting output stream for port " + this.port);
                output = client.getOutputStream();
            } catch (IOException e) {
                log.error("Failed to open output stream for server: " + this.getId() + " for reason: " + e.toString());
                this.shouldSeekClients = false;
            }
        }
        return output;
    }

    private Servlet initializeServlet(InputStream input, OutputStream output) {
        Servlet servlet = null;
        if (this.shouldSeekClients) {
            servlet = new Servlet(input, output, this.Integrity, this.getId(), this.port);
        }
        return servlet;
    }

    private void handleClient(Servlet servlet) {
        if (this.shouldSeekClients) {
            servlet.handleClient();
        }
    }

    private void cleanUpClient(Servlet servlet) {
        if (this.shouldSeekClients) {
            servlet.cleanUp();
        }
    }

    private void closeClient(Socket client) {
        if (client != null) {
            try {
                client.close();
            } catch (IOException e) {
                log.error("Could not close client " + client.toString() + " on port " + this.port + " for server " + this.getId() + " for reason: " + e.toString());
                this.interrupt();
            }
        }
    }

    private void closeServer(ServerSocket server) {
        try {
            server.close();
        } catch (IOException e) {
            log.error("Could not close server " + this.getId() + " on port " + this.port + " for reason: " + e.toString());
            this.interrupt();
        }
    }

}