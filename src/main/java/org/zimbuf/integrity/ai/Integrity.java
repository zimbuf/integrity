/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.ai;

import org.zimbuf.integrity.lang.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Integrity {
    private SyntaxTree syntaxTree;
    private SubjectClauses subjects;

    public Integrity() {
        this.syntaxTree = new SyntaxTree();
        this.initSyntaxTree();
        this.subjects = new SubjectClauses();
    }

    private void initSyntaxTree() {
        try {
            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("where|what|who",1, "it is"), 0);
            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("how|why",2, "it is"), 0);

            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("where", 3, "located"), 1);
            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("what|who", 4, ""), 1);

            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("what", 5, ""), 4);
            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("who", 6, ""), 4);

            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("why", 7, "because"), 2);
            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("how", 8, ""), 2);

            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("think", 9, "my opinion that", Topics.OPINION), 5);
            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("think|is", 10, "my opinion that", Topics.OPINION), 6);

            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("make|made|done|created|constructed", 11, "created by", Topics.PROCESS), 8);
            this.syntaxTree.addLeaf(new SyntaxTreeLeaf("feel|feels|feelings", 12, "their feelings that", Topics.STATUS), 8);

        } catch (Exception e) {
            System.err.print(e.getMessage());
        }
    }

    public String generateMessage(String content) {
        SyntaxTreeResponse resp = this.syntaxTree.traverse(content);
        return catchError(groomOutput(resp.getResponse()
                        .concat(" ")
                        .concat(this.subjects
                                .findTopic(resp, content))));
    }

    private String groomOutput(String tentative) {
        Pattern pat = Pattern.compile(" {2}", Pattern.CASE_INSENSITIVE);
        String replacement = tentative;
        Matcher mat = pat.matcher(replacement);
        while(mat.find()) {
            replacement = mat.replaceFirst(" ");
            mat = pat.matcher(replacement);
        }
        return replacement.trim();
    }

    private String catchError(String tentative) {
        var regex = Pattern.compile(Constants.ERR);
        var containsError = regex.matcher(tentative).find();
        if (containsError) {
            return "I do not support that query";
        }
        return tentative;
    }
}