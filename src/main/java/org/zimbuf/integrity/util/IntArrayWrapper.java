/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.util;

public class IntArrayWrapper {
    private int[] array;
    private int size;
    private int currentEmptyIndex;

    public IntArrayWrapper(int[] array) {
        this.array = array;
        this.size = array.length;
        this.currentEmptyIndex = 0;
    }

    public boolean contains(int value) {
        for(int x = 0; x < this.array.length; x++) {
            if (this.array[x] == value) {
                return true;
            }
        }
        return false;
    }

    public void add(int value) {
        if(this.currentEmptyIndex != this.size) {
            this.array[this.currentEmptyIndex] = value;
            this.currentEmptyIndex++;
        }
    }

    public boolean isFull() {
        return this.size == this.currentEmptyIndex;
    }
}
