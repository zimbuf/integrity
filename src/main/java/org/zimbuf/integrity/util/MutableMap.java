/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.util;

import java.util.ArrayList;

public class MutableMap<K, V> {
    protected ArrayList<K> keys;
    protected ArrayList<V> values;

    public MutableMap() {
        this.keys = new ArrayList<>();
        this.values = new ArrayList<>();
    }

    public int size() {
        return this.keys.size();
    }

    public boolean isEmpty() {
        return keys.size() == 0;
    }

    public V getValue(K key) {
        for(int x = 0; x < this.keys.size(); x++) {
            if(this.keys.get(x) == key) {
                return this.values.get(x);
            }
        }
        return null;
    }

    public void put(K key, V value) {
        if (this.keys.contains(key)) {
            this.values.set(this.keys.indexOf(key), value);
        }
        else {
            this.keys.add(key);
            this.values.add(value);
        }
    }

    public void delete(K key) {
        this.values.remove(this.keys.indexOf(key));
        this.keys.remove(key);
    }
}
