/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.util;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class JSON<T> {
    private T contents;

    public JSON(Class<T> JSONable, String fileName) throws IOException {
        String content = this.getFileContents(fileName);
        T object = this.convertFromJSON(content, JSONable);
        this.contents = object;
    }

    private T convertFromJSON(String content, Class<T> JSONable) {
        Gson converter = new Gson();
        return converter.fromJson(content, JSONable);
    }

    private String getFileContents(String fileName) throws IOException {
        byte[] bytes = this.getClass().getClassLoader().getResourceAsStream(fileName).readAllBytes();
        return new String(bytes);
    }

    public T get() {
        return this.contents;
    }
}
