/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.lang;

import org.zimbuf.integrity.util.IntArrayWrapper;
import lombok.Getter;

public class SyntaxTree {
    @Getter
    private SyntaxTreeLeaf root;
    private int size;

    public SyntaxTree() {
        this.root = new SyntaxTreeLeaf(0);
        this.size = 1;
    }

    public SyntaxTreeResponse traverse(String sentence) {
        SyntaxTreeLeaf current = this.root;
        String response = "";
        boolean invalid = false;
        Topics topic = Topics.NONE;
        while(!invalid) {
            if(current.isLeft() && current.getLeft().matches(sentence)) {
                response = response.concat(" ")
                        .concat(current.getLeft()
                                .getResponse());
                topic = current.getLeft()
                        .hasTopic() ?
                        current.getLeft()
                        .getTopic() :
                        Topics.NONE;
                current = current.getLeft();
            }
            else if (current.isRight() && current.getRight().matches(sentence)) {
                response = response.concat(" ")
                        .concat(current.getRight()
                                .getResponse());
                topic = current.getRight()
                        .hasTopic() ?
                        current.getRight()
                        .getTopic() :
                        Topics.NONE;
                current = current.getRight();
            }
            else {
                invalid = true;
            }
        }
        if(topic == Topics.NONE) {
            return new SyntaxTreeResponse(Constants.ERR, topic);
        }
        else {
            return new SyntaxTreeResponse(response, topic);
        }
    }

    public void addLeaf(SyntaxTreeLeaf leaf, int parentId) throws Exception {
        SyntaxTreeLeaf parent = this.searchByDepth(parentId);
        if(!parent.isLeft()) {
            parent.setLeft(leaf);
            leaf.setParent(parent);
            this.size++;
        }
        else if(!parent.isRight()) {
            parent.setRight(leaf);
            leaf.setParent(parent);
            this.size++;
        }
        else {
            throw new Exception("No valid parent found for leaf in binary AST");
        }
    }

    private SyntaxTreeLeaf searchByDepth(int id) {
        IntArrayWrapper discovered = new IntArrayWrapper(new int[this.size]);
        SyntaxTreeLeaf current = this.root;
        while(!discovered.isFull() && current != null && !current.is(id)) {
            if (current.isLeft() && !discovered.contains(current.getLeft().getId())) {
                current = current.getLeft();
                discovered.add(current.getId());
            }
            else if (current.isRight()  && !discovered.contains(current.getRight().getId())) {
                current = current.getRight();
                discovered.add(current.getId());
            }
            else {
                current = current.getParent();
            }
        }
        return current;
    }
}
