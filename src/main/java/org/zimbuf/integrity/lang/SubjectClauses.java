/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.lang;

import org.zimbuf.integrity.util.ImmutableMap;
import org.zimbuf.integrity.util.JSON;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubjectClauses {
    private ImmutableMap<String, String> opinions;
    private ImmutableMap<String, String> status;
    private ImmutableMap<String, String> process;

    public SubjectClauses() {
        JSON<Subject> keywords = this.initializeJson("keywords.json");
        JSON<Subject> clauses = this.initializeJson("clauses.json");
        this.opinions = this.initializeMap(keywords.get().getOpinions(), clauses.get().getOpinions());
        this.status = this.initializeMap(keywords.get().getStatus(), clauses.get().getStatus());
        this.process = this.initializeMap(keywords.get().getProcess(), clauses.get().getProcess());
    }

    private ImmutableMap<String, String> initializeMap(String[] keywords, String[] clauses) {
        try {
            return new ImmutableMap<>(keywords, clauses);
        } catch (Exception e) {
            System.err.println("Could not create ImmutableMap for reason: ".concat(e.toString()));
            System.exit(-1);
        }
        return null;
    }

    private JSON<Subject> initializeJson(String location) {
        try {
            return new JSON<>(Subject.class, location);
        } catch (IOException e) {
            System.err.println("Could not find resource for keywords or clauses for reason: ".concat(e.toString()));
            System.exit(-1);
        }
        return null;
    }

    public String findTopic(SyntaxTreeResponse resp, String content) {
        switch (resp.getTopic()) {
            case OPINION:
                return this.siftThroughMaps(content, this.opinions);
            case STATUS:
                return this.siftThroughMaps(content, this.status);
            case PROCESS:
                return this.siftThroughMaps(content, this.process);
            default:
                return Constants.ERR;
        }
    }

    private String siftThroughMaps(String content, ImmutableMap<String, String> map) {
        String[] keys = map.getKeys();
        for(String key : keys) {
            Pattern pat = Pattern.compile(key, Pattern.CASE_INSENSITIVE);
            Matcher match = pat.matcher(content);
            if (match.find()) {
                return map.at(key);
            }
        }
        return Constants.ERR;
    }

}
