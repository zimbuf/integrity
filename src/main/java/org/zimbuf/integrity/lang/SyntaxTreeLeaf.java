/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.lang;

import lombok.Getter;
import lombok.Setter;

import java.util.regex.*;

public class SyntaxTreeLeaf {
    @Getter
    @Setter
    private SyntaxTreeLeaf left;
    @Getter
    @Setter
    private SyntaxTreeLeaf right;
    @Setter
    @Getter
    private SyntaxTreeLeaf parent;
    private Pattern expression;
    @Getter
    private String response;
    @Getter
    private Topics topic;
    @Getter
    private int id;

    public SyntaxTreeLeaf(String regularExpression, int id, String response) {
        this.expression = Pattern.compile(regularExpression, Pattern.CASE_INSENSITIVE);
        this.id = id;
        this.response = response;
    }

    public SyntaxTreeLeaf(String regularExpression, int id, String response, Topics topic) {
        this.expression = Pattern.compile(regularExpression, Pattern.CASE_INSENSITIVE);
        this.id = id;
        this.response = response;
        this.topic = topic;
    }

    public SyntaxTreeLeaf(int id) {
        this.expression = Pattern.compile("", Pattern.CASE_INSENSITIVE);
        this.response = "";
        this.id = id;
    }

    public boolean is(int id) {
        return this.id == id;
    }

    public boolean matches(String target) {
        return expression.matcher(target).find();
    }

    public boolean hasTopic() {
        return this.topic != null;
    }

    public boolean isLeft() {
        return this.left != null;
    }

    public boolean isRight() {
        return this.right != null;
    }
}
