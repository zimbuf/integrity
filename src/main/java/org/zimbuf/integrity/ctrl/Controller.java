/*
Copyright 2019 zimbuf

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


package org.zimbuf.integrity.ctrl;

import org.zimbuf.integrity.ai.Integrity;
import org.zimbuf.integrity.srv.ServerHost;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

public class Controller {
    private Integrity integrity;
    private ServerHost host;
    private Scanner input;
    private boolean shouldRun;

    private void initLogProperties() {
        Properties prop = new Properties(System.getProperties());
        try {
            InputStream in = getClass()
                    .getClassLoader()
                    .getResource("log4j.properties")
                    .openStream();
            prop.load(in);
        } catch (Exception e) {
            System.err.println("Logging failed to be initialized. Log contents will now be posted to the console session");
        }
    }

    public Controller() {
        this.initLogProperties();
        this.integrity = new Integrity();
        this.host = new ServerHost(this.integrity, 10, 5000);
        this.input = new Scanner(System.in);
        this.shouldRun = true;
    }

    public void initializeApp() {
        try {
            host.start();
        } catch (Exception e) {
            System.err.println("Abnormal program termination for reason: " + e.toString());
        }
    }

    public void acceptCommands() {
        while(this.shouldRun) {
            System.out.println("$ ");
            String command = this.input.nextLine();
            boolean result = this.executeCommand(command);
            this.outputResult(result);
        }
    }

    private boolean executeCommand(String command) {
        if(command.equals("EXIT")) {
            System.out.println("Shutting down...");
            this.host.interrupt();
            this.shouldRun = false;
        }
        else {
            return false;
        }
        return true;
    }

    private void outputResult(boolean result) {
        if (result) {
            System.out.println("Command executed successfully");
        }
        else {
            System.out.println("Command not executed. Please verify keyword syntax");
        }
    }
}
